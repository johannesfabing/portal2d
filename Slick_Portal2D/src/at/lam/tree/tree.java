package at.lam.tree;

public abstract class tree {
	private int maxSize;
	private int maxDiameter;
	
	public tree(int maxSize, int maxDiameter) {
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public int getMaxDiameter() {
		return maxDiameter;
	}

	public void setMaxDiameter(int maxDiameter) {
		this.maxDiameter = maxDiameter;
	}
	
	
	

}
