package portal2d.actions;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;

public class KeyPressed {
	@SuppressWarnings("unused")
	public static boolean Up(GameContainer gc) {
		return gc.getInput().isKeyDown(Input.KEY_UP);
	}
	
	@SuppressWarnings("unused")
	public static boolean Left(GameContainer gc) {
		return gc.getInput().isKeyDown(Input.KEY_LEFT);
	}
	
	@SuppressWarnings("unused")
	public static boolean Right(GameContainer gc) {
		return gc.getInput().isKeyDown(Input.KEY_RIGHT);
	}

	public static boolean Down(GameContainer gc) {
		return gc.getInput().isKeyDown(Input.KEY_DOWN);
	}
}
