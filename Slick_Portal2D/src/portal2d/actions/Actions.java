package portal2d.actions;

import org.newdawn.slick.geom.Point;

public class Actions {
	public static Point Move(Point point, float angle, double speed, int delta) {
		point.setX((float) (point.getX() + (Math.cos(angle * Math.PI / 180) * speed * delta)));
		point.setY((float) (point.getY() + (Math.sin(angle * Math.PI / 180) * speed * delta)));
		return point;
	}

	public static float MoveDown(float y, double speed, int delta) {
		return (float) (y + speed * delta);
	}

	public static float MoveUp(float y, double speed, int delta) {
		return (float) (y + speed * delta);
	}

	public static float MoveRight(float x, double speed, int delta) {
		return (float) (x + speed * delta);
	}

	public static float MoveLeft(float x, double speed, int delta) {
		return (float) (x - speed * delta);
	}

	public static Point MovePoint(Point position, float angle, int delta, double speed) {
		switch ((int) angle) {
		case 0:
			position.setX(Actions.MoveRight(position.getX(), speed, delta));
			break;
		case 90:
			position.setY(Actions.MoveUp(position.getY(), speed, delta));
			break;
		case 180:
			position.setX(Actions.MoveLeft(position.getX(), speed, delta));
			break;
		case 270:
			position.setY(Actions.MoveDown(position.getY(), speed, delta));
			break;
		default:
			position = Actions.Move(position, angle, speed, delta);
			break;
		}

		return position;
	}

}
