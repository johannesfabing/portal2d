package portal2d.actions;

import org.newdawn.slick.geom.Point;

public class Gravity {

	static double velocity = 0.00981;
	
	
	public static double getspeed(int delta, double speed)
	{
		speed = speed + velocity * delta;
		
		return speed;
	}
}
