package portal2d.main;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import portal2d.sprites.Charakter;

public class Portal2d extends BasicGame {

	private List<Actors> actors;

	public Portal2d() {
		super("Portal2D");
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		Charakter Loligirl = new Charakter();
		actors = new ArrayList<Actors>();
		actors.add(Loligirl);
	}

	public static void main(String[] args) {
		try {
			AppGameContainer gc = new AppGameContainer(new Portal2d());
			gc.setDisplayMode(800, 600, false);
			gc.start();
			gc.setVSync(true);
			gc.setTargetFrameRate(120);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for (Actors actor : actors) {
			actor.update(delta, gc);
		}
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		for (Actors actor : actors) {
			actor.render(g);
		}

	}
}
