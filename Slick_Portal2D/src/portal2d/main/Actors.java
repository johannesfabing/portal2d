package portal2d.main;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public interface Actors {
	
	public void update(int delta, GameContainer gc) throws SlickException;
	
	public void render(Graphics g);
}
