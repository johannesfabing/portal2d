package portal2d.sprites;

import portal2d.actions.Actions;
import portal2d.actions.Gravity;
import portal2d.actions.KeyPressed;
import portal2d.main.Sprites;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Point;

public class Charakter implements Sprites{

	double speed = 0;
	boolean isOnGround = false;
	boolean isJumping = false;
	double movespeed = 0.5;
	float jumpspeed = (float) -2;
	Point location = new Point(0,500);
	
	@Override
	public void update(int delta, GameContainer gc) throws SlickException {
		Move(delta, gc);
		System.out.println(delta);
	}

	@Override
	public void render(Graphics g) {
		Draw(g);
	}
	
	public void Jump(Point position, int delta) {
		if(speed<0) {
			position.setY(Actions.MoveUp(position.getY(),speed, delta));
		}
		else {
			isJumping = false;
		}
	}
	
	public void Move(int delta, GameContainer gc) {
		
		if(isJumping) {
			Jump(location, delta);
		}
		
		if(KeyPressed.Right(gc)) {
			Actions.MovePoint(location, 0, delta, movespeed);
		}
		
		if(KeyPressed.Left(gc)) {
			Actions.MovePoint(location, 180, delta, movespeed);
		}
		
		if(KeyPressed.Up(gc) && isOnGround) {
			isJumping = true;
			isOnGround = false;
			speed = jumpspeed;
			Jump(location, delta);
		}
		
		if(location.getY() >= 500){
			isJumping = false;
			isOnGround = true;
			speed = 0;
		}
		
		if(isOnGround == false){
			speed = Gravity.getspeed(delta, speed);
			Actions.MovePoint(location, 270, delta, speed);
		}
		
	}
	
	public void Draw(Graphics g) {
		g.fillRect(location.getX(), location.getY(), 40, 100);
	}
	
}
